import * as axios from 'axios';
import {
  ADD_NEW_EVENT_STARTING,
  ADD_NEW_EVENT_REJECT,
  ADD_NEW_EVENT_RESOLVE,
} from './actionTypes';

export const addNewEventStarting = (payload) => ({
  type: ADD_NEW_EVENT_STARTING,
  payload,
});

export const addNewEventResolve = (payload) => ({
  type: ADD_NEW_EVENT_RESOLVE,
  payload,
});
export const addNewEventReject = (payload) => ({
  type: ADD_NEW_EVENT_REJECT,
  payload,
});
export const addNewEventAction = (data) => (dispatch) => {
  dispatch(addNewEventStarting(data));
  dispatch(addNewEventResolve(data));
  axios
    .post('https://www.ya.ru/', data)
    .then((res) => {
      dispatch(addNewEventResolve(res.data));
    })
    .catch((error) => {
      dispatch(addNewEventReject(error));
    });
};
