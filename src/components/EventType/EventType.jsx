/* eslint react/prop-types: 0 */
import React from 'react';
import styles from './EventType.module.css';

const EventType = (props) => {
  const { color, children } = props;
  return (
    <div className={styles.eventTypeItem}>
      <span
        className={styles.dot}
        style={{ backgroundColor: color }}
      />
      <span className={styles.description}>{children}</span>
    </div>
  );
};

export default EventType;
