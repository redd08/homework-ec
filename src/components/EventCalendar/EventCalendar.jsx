/* eslint react/prop-types: 0 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { Spin, Alert } from 'antd';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import { addNewEventAction } from '../../actions/eventActions';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import EventLegend from '../EventLegend/EventLegend';
import styles from './EventCalendar.module.css';
import 'moment/locale/ru';

moment.locale('ru');
const localizer = momentLocalizer(moment);

const minTime = new Date();
const maxTime = new Date();
minTime.setHours(8, 0, 0);
maxTime.setHours(18, 0, 0);
const messages = {
  week: 'Неделя',
  agenda: 'Повестка дня',
  today: 'Сегодня',
  previous: 'Назад',
  next: 'Вперед',
};

class EventCalendar extends Component {
  // eslint-disable-next-line class-methods-use-this
  eventStyleGetter() {
    const style = {
      backgroundColor: '#FFD183',
      color: 'black',
      fontWeight: 'bold',
      border: '0px',
      display: 'block',
    };
    return {
      style,
    };
  }

  render() {
    const {
      events, err, isBusy, addNewEvent,
    } = this.props;
    return (
      <div className={styles.EventCalendar}>
        {err && (
          <Alert
            message="Ошибка"
            description="Ошибка добавления мероприятия"
            type="error"
            closable
          />
        )}
        {isBusy ? (
          <Spin />
        ) : (
          <>
            <div className={styles.Calendar}>
              <Calendar
                localizer={localizer}
                defaultView="week"
                titleAccessor="comment"
                events={events}
                startAccessor="start"
                endAccessor="end"
                style={{ height: 600, width: 900 }}
                views={['week', 'agenda']}
                min={minTime}
                max={maxTime}
                messages={messages}
                eventPropGetter={this.eventStyleGetter}
                toolbar={false}
              />
            </div>
            <div className={styles.EventLegend}>
              <EventLegend addNewEvent={addNewEvent} />
            </div>
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  events: state.event.events,
  isBusy: state.event.isBusy,
  err: state.event.err,
});
const mapDispatchToProps = (dispatch) => bindActionCreators(
  { addNewEvent: addNewEventAction }, dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(EventCalendar);
