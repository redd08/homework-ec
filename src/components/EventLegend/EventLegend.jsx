import React, { Component } from 'react';
import { Form, Modal, Button, DatePicker, Input } from 'antd';
import styles from './EventLegend.module.css';
import EventType from '../EventType/EventType';
const { TextArea } = Input;

class EventLegend extends Component {
  state = {
    visibleModal: false,
    start: null,
    end: null,
    title: null,
    confirmLoading: false,
  };
  showModal = () => {
    this.setState({
      visibleModal: true,
    });
  };
  onOk = () => {
    this.setState({
      confirmLoading: true,
    });
    this.props.addNewEvent(this.state);
    this.setState({
      confirmLoading: false,
      visibleModal: false,
      start: null,
      end: null,
      title: null,
    });
    console.log(this.state);
  };
  onSetStart = value => {
    this.setState({
      start: value._d,
    });
  };
  onSetEnd = value => {
    this.setState({ end: value._d });
  };
  onChangeTextArea = e => {
    this.setState({ title: e.target.value });
  };
  onCancel = () => {
    this.setState({
      visibleModal: false,
    });
  };
  render() {
    const { visibleModal, confirmLoading, title, start, end } = this.state;
    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };
    return (
      <div className={styles.wrapper}>
        <>
          <div className={styles.typelist}>
            <EventType color="#fff">Свободное время</EventType>
            <EventType color="#01b775">
              Выделено для текущей организации
            </EventType>
            <EventType color="#06895f">
              Выделено для другой организации
            </EventType>
            <EventType color="#2891e8">Выделено вами</EventType>
            <EventType color="#0055a6">Выделено менеджером</EventType>
            <EventType color="#fdba39">Не рабочее время (перерыв)</EventType>
          </div>
          <Button type="primary" onClick={this.showModal}>
            + Добавить мероприятие
          </Button>
          <Modal
            title="Добвить новое мероприятие"
            visible={visibleModal}
            onOk={this.onOk}
            confirmLoading={confirmLoading}
            onCancel={this.onCancel}
          >
            <Form name="addEvent" {...layout}>
              <Form.Item label="Start" name="eventStart">
                <DatePicker showTime value={start} onOk={this.onSetStart} />
              </Form.Item>
              <Form.Item label="End" name="eventEnd">
                <DatePicker showTime value={end} onOk={this.onSetEnd} />
              </Form.Item>
              <Form.Item label="Comment" name="comment">
                <TextArea
                  rows={4}
                  onChange={this.onChangeTextArea}
                  value={title}
                />
              </Form.Item>
            </Form>
          </Modal>
        </>
      </div>
    );
  }
}

export default EventLegend;
