import { combineReducers } from 'redux';
import { addNewEventReducer } from './eventReducer';

const rootReducer = combineReducers({
  event: addNewEventReducer,
});
export default rootReducer;
