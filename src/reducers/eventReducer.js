import {
  ADD_NEW_EVENT_STARTING,
  ADD_NEW_EVENT_RESOLVE,
  ADD_NEW_EVENT_REJECT,
} from '../actions/actionTypes';

export const initState = {
  events: [
    {
      id: 1,
      start: new Date('2020-06-24T09:15:00+04:00'),
      end: new Date('2020-06-24T14:00:00+04:00'),
      comment: 'Комметарий к событию. Он может быть довольно длинным',
    },
    {
      id: 2,
      start: new Date('2020-06-23T11:00:00+04:00'),
      end: new Date('2020-06-23T15:30:00+04:00'),
      comment: null,
    },
  ],
  err: null,
  isBusy: false,
};

export const addNewEventReducer = (state = initState, action) => {

  switch (action.type) {
    case ADD_NEW_EVENT_STARTING:
      return {
        ...state,
        isBusy: true,
        err: null,
      };
    case ADD_NEW_EVENT_RESOLVE:
      const newEvent = {
        start: action.payload.start,
        end: action.payload.end,
        comment: action.payload.title,
        id: 9999,
      };
      return {
        ...state,
        events: [...state.events, newEvent],
        isBusy: false,
        err: null,
      };
    case ADD_NEW_EVENT_REJECT:
      return {
        ...state,
        err: action.payload,
        isBusy: false,
      };
    default:
      return state;
  }
};
